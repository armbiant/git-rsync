"""Sphinx configuration."""
from datetime import datetime


project = "Git Rsync"
author = "Daniel Gomez"
copyright = f"{datetime.now().year}, {author}"
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_click",
    "sphinxarg.ext",
]
autodoc_typehints = "description"
html_theme = "furo"
