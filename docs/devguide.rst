Developer Guide
===============

Build
-----

This project uses `poetry <https://python-poetry.org>` for pdependency
management and packaging in Python.

.. code-block::

    ./scripts/poetry-build.sh

    Creating virtualenv git-rsync-APuiIKTR-py3.10 in /root/.cache/pypoetry/virtualenvs
    Building git-rsync (0.2.1)
      - Building sdist
      - Built git-rsync-0.2.1.tar.gz
      - Building wheel
      - Built git_rsync-0.2.1-py3-none-any.whl

Output packages (``*.whl`` and ``*.tar.gz``) will be found in ``dist/``.

Install locally
---------------

Use the previous output packages to install ``git-rsync`` locally:

.. code-block::

    pip install --force-reinstall dist/git_rsync-0.2.1-py3-none-any.whl

    Defaulting to user installation because normal site-packages is not writeable
    Processing ./dist/git_rsync-0.2.1-py3-none-any.whl
    Collecting click<9.0.0,>=8.0.1
      Downloading click-8.1.3-py3-none-any.whl (96 kB)
         ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 96.6/96.6 kB 1.4 MB/s eta 0:00:00
    Collecting colorlog<7.0.0,>=6.6.0
      Downloading colorlog-6.7.0-py2.py3-none-any.whl (11 kB)
    Installing collected packages: colorlog, click, git-rsync
      Attempting uninstall: colorlog
        Found existing installation: colorlog 6.6.0
        Uninstalling colorlog-6.6.0:
          Successfully uninstalled colorlog-6.6.0
    Successfully installed click-8.1.3 colorlog-6.7.0 git-rsync-0.2.1


Docker image build
------------------

Remember to cleanup the ``dist/`` directory and build before building the
``git-rsync:local`` container image.

.. code-block::

    ./scripts/docker-build.sh

    Sending build context to Docker daemon  96.35MB
    Step 1/5 : FROM alpine:latest
     ---> c059bfaa849c
    Step 2/5 : RUN apk add git py3-pip
     ---> Using cache
     ---> 188c090f20ea
    Step 3/5 : ADD dist /mnt/dist
     ---> 80a688332a9c
    Step 4/5 : RUN pip3 install --force-reinstall /mnt/dist/git_rsync*.whl
     ---> Running in 9a54e2736563
    Processing /mnt/dist/git_rsync-0.2.1-py3-none-any.whl
    Collecting colorlog<7.0.0,>=6.6.0
      Downloading colorlog-6.7.0-py2.py3-none-any.whl (11 kB)
    Collecting click<9.0.0,>=8.0.1
      Downloading click-8.1.3-py3-none-any.whl (96 kB)
    Installing collected packages: colorlog, click, git-rsync
    Successfully installed click-8.1.3 colorlog-6.7.0 git-rsync-0.2.1
    Removing intermediate container 9a54e2736563
     ---> 102ba79fee69
    Step 5/5 : RUN rm -rf /mnt/dist
     ---> Running in 13a8922ebf4b
    Removing intermediate container 13a8922ebf4b
     ---> f2f1124681d0
    Successfully built f2f1124681d0
    Successfully tagged git-rsync:local

You can also use different docker tag:

.. code-block::

    ./scripts/docker-build.sh testing

    Sending build context to Docker daemon  96.35MB
    Step 1/5 : FROM alpine:latest
     ---> c059bfaa849c
    Step 2/5 : RUN apk add git py3-pip
     ---> Using cache
     ---> 188c090f20ea
    Step 3/5 : ADD dist /mnt/dist
     ---> Using cache
     ---> 80a688332a9c
    Step 4/5 : RUN pip3 install --force-reinstall /mnt/dist/git_rsync*.whl
     ---> Using cache
     ---> 102ba79fee69
    Step 5/5 : RUN rm -rf /mnt/dist
     ---> Using cache
     ---> f2f1124681d0
    Successfully built f2f1124681d0
    Successfully tagged git-rsync:testing

Development environment
-----------------------

Get into the development environment by using the csmOS Python container
development image.

.. code-block::

    ./scripts/run-python-env.sh

    root@b5ac85a653b3:/home/daniel/csmos/pypi/git-rsync#

After jumping into the container, you can run ``poetry`` and ``nox`` tools among others.

GitLab Container Registry
-------------------------

Using the `GitLab Container Registry`_ requires authentication. Just run:

.. code-block::

    docker login registry.gitlab.com -u <username> -p <token>

.. _GitLab Container Registry: https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-with-the-container-registry
