#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-only
#
# Launch Python Development Environment based on Poetry.
#
# Copyright (C) 2022 Daniel Gomez
# Author: Daniel Gomez <dagmcr@gmail.com>
set -e

docker run --rm -it \
-w $PWD \
-v $PWD:$PWD \
--entrypoint /bin/bash \
registry.gitlab.com/csmos/containerhub/python-dev
