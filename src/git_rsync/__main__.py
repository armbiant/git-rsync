"""Git Rsync: Syncronize git references between repositories.

Copyright (C) 2022 Daniel Gomez
Author: Daniel Gomez <daniel@gmail.com>

Syncronizes a local git repository with upstream references.
"""
import argparse
import configparser
import logging
import os
import subprocess
import sys
import tempfile
import threading

import colorlog

from . import __version__


def _logger_conf(args: argparse.Namespace) -> None:
    """Setup the logging environment."""
    log = logging.getLogger()
    log.setLevel(logging.INFO)
    format_str = "%(asctime)s - %(levelname)-8s - %(message)s"
    date_format = "%Y-%m-%d %H:%M:%S"
    formatter = logging.Formatter(format_str, date_format)
    if os.isatty(2):
        cformat = "%(log_color)s" + format_str
        colors = {
            "DEBUG": "reset",
            "INFO": "bold_black",
            "WARNING": "bold_yellow",
            "ERROR": "bold_red",
            "CRITICAL": "bold_red",
        }
        formatter = colorlog.ColoredFormatter(cformat, date_format, log_colors=colors)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    log.addHandler(stream_handler)
    log.setLevel(logging.INFO)
    _logger_level(args)
    return


def _logger_level(args: argparse.Namespace) -> None:
    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)
    return


def _conf_init(inifile: str) -> configparser.ConfigParser:
    conf = configparser.ConfigParser()
    conf.read(inifile)
    return conf


def _conf_sections(conf: configparser.ConfigParser) -> list[str]:
    sections = []
    for s in conf.sections():
        if "upstream" in conf[s] and "mirror" in conf[s]:
            sections.append(s)
    return sections


def _run(
    cmd: list[str], environ: dict[str, str] = {}, comm: bool = True
) -> subprocess.CompletedProcess[bytes]:
    try:
        logging.debug(f"{' '.join(cmd)}")
        p = subprocess.run(cmd, capture_output=True, check=True, env=environ)
        if comm:
            stdout = p.stdout
            stderr = p.stderr
            if stdout:
                logging.debug(stdout.decode("utf-8"))
            if stderr:
                logging.debug(stderr.decode("utf-8"))
            logging.debug(f"Return code: {p.returncode}")
        return p
    except subprocess.CalledProcessError as exc:
        sys.exit(exc.returncode)


def _popen(
    cmd: list[str], environ: dict[str, str] = {}, comm: bool = True
) -> subprocess.Popen[bytes]:
    try:
        logging.debug(f"{' '.join(cmd)}")
        p = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=environ
        )
        if comm:
            stdout, stderr = p.communicate()
            if stdout:
                logging.debug(stdout.decode("utf-8"))
            if stderr:
                logging.debug(stderr.decode("utf-8"))
            logging.debug(f"Return code: {p.returncode}")
        return p
    except subprocess.CalledProcessError as exc:
        sys.exit(exc.returncode)


def _git_config_push_refspec(refspec: str) -> None:
    cmd = [
        "git",
        "config",
        "--add",
        "remote.origin.push",
        f"{refspec}",
    ]
    _run(cmd)


def _git_config_push(repo: str, refs: str, args: argparse.Namespace) -> None:
    """Configure mirror repository.

    Steps:
    - Disable git mirror
    - Update remote url (include oauth2:token if provided)
    - Add refs to push
    Note: Add 'refs/head/*' and 'refs/tags/*' if refs not define in conf file.
    """
    # Example:
    # root@a8fac57fc8b6:/tmp/tmpe1wv3y80# cat config
    # [core]
    #         repositoryformatversion = 0
    #         filemode = true
    #         bare = true
    # [remote "origin"]
    #         url = git@gitlab.com:csmos/mirrors/musl.git
    #         fetch = +refs/*:refs/*
    #         mirror = false
    #         push = +refs/heads/*:refs/heads/*
    #         push = +refs/tags/*:refs/tags/*

    # Disable mirror option
    cmd = ["git", "config", "remote.origin.mirror", "false"]
    _run(cmd)

    # Update origin repo from upstream to mirror
    if args.mirror_token:
        repo = repo.replace("https://", f"https://oauth2:{args.mirror_token}@")
    cmd = ["git", "remote", "set-url", "origin", f"{repo}"]
    _run(cmd)

    if refs:
        for r in refs.split(","):
            _git_config_push_refspec(r)
        return

    # Add push refs: heads
    refspec = "+refs/heads/*:refs/heads/*"
    _git_config_push_refspec(refspec)

    # Add push refs: tags
    refspec = "+refs/tags/*:refs/tags/*"
    _git_config_push_refspec(refspec)
    return


def _git_ls_remote(args: argparse.Namespace, repo: str) -> int:
    if args.upstream_token:
        repo = repo.replace("https://", f"https://oauth2:{args.upstream_token}@")
    cmd = _add_verbose(args, ["git", "ls-remote", repo])
    p = _run(cmd)
    if p.returncode:
        logging.error(f"Invalid repository: {repo}")
    return p.returncode


def _add_verbose(args: argparse.Namespace, cmd: list[str]) -> list[str]:
    if args.debug:
        cmd.append("--verbose")
    return cmd


def _git_push(args: argparse.Namespace) -> subprocess.Popen[bytes]:
    cmd = _add_verbose(args, ["git", "remote"])
    _popen(cmd)

    cmd = _add_verbose(args, ["git", "push"])
    return _popen(cmd)


def _git_clone(
    args: argparse.Namespace, upstream: str, dir: str
) -> subprocess.Popen[bytes]:
    if args.upstream_token:
        upstream = upstream.replace(
            "https://", f"https://oauth2:{args.upstream_token}@"
        )
    cmd = _add_verbose(args, ["git", "clone", "--mirror", "--progress", upstream, dir])
    return _popen(cmd)


def _sync_repo(
    conf: configparser.ConfigParser, args: argparse.Namespace, s: str
) -> None:
    upstream = conf[s]["upstream"]
    mirror = conf[s]["mirror"]
    refs = ""
    if conf[s]["refs"]:
        refs = conf[s]["refs"]

    # Valid repo
    ret = _git_ls_remote(args, upstream)
    if ret:
        return

    # Sync upstream repo
    with tempfile.TemporaryDirectory() as td:
        cwd = os.getcwd()
        logging.debug(f"'{s}' clone done")
        _git_clone(args, upstream, td)
        os.chdir(td)
        _git_config_push(mirror, refs, args)
        logging.debug(f"'{s}' sync config done")
        _git_push(args)
        logging.debug(f"'{s}' push done")
        os.chdir(cwd)

    logging.info(f"'{s}' sync completed")
    return


def _start_thread_dbg(conf: configparser.ConfigParser, s: str) -> None:
    logging.info(f"Syncing '{s}'...")
    upstream = conf[s]["upstream"]
    mirror = conf[s]["mirror"]
    logging.debug(f"\tUpstream: {upstream}")
    logging.debug(f"\tMirror:   {mirror}")
    return


def _start_thread(
    conf: configparser.ConfigParser,
    args: argparse.Namespace,
    s: str,
    th: list[threading.Thread],
) -> threading.Thread:
    _start_thread_dbg(conf, s)
    t = threading.Thread(
        target=_sync_repo,
        args=(
            conf,
            args,
            s,
        ),
    )
    t.start()
    th.append(t)
    return t


def _task_check_completed(
    conf: configparser.ConfigParser, t: threading.Thread, th: list[threading.Thread]
) -> None:
    t.join(1)  # Check if a thread has finished with 1s of timeout
    if not t.is_alive():  # Thread has finished
        th.remove(t)


def _task_launcher(conf: configparser.ConfigParser, args: argparse.Namespace) -> None:
    """Launches a thread per repo with a max number of jobs in parallel."""
    th = list[threading.Thread]()
    for s in conf.sections():
        _start_thread(conf, args, s, th)
        while len(th) >= args.jobs:  # Wait until finish one of the running threads
            for t in th:
                _task_check_completed(conf, t, th)
    while len(th):
        for t in th:
            _task_check_completed(conf, t, th)


def _parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Git Reference Sync tool")
    parser.add_argument(
        "-d",
        "--debug",
        help="enable debug output",
        action="store_true",
    )
    parser.add_argument(
        "-j",
        "--jobs",
        type=int,
        default=1,
        help="number of jobs at once",
    )
    parser.add_argument(
        "--upstream-token",
        help="Upstream OAuth2 Access Token",
    )
    parser.add_argument(
        "--mirror-token",
        help="Mirror OAuth2 Access Token",
    )
    parser.add_argument(
        "-v",
        "--version",
        help="%(prog)s version",
        action="version",
        version=f"%(prog)s {__version__}",
    )
    parser.add_argument(
        "--conf",
        help="configuration file",
    )
    return parser


def main() -> None:
    """Git Sync entry point."""
    p = _parser()
    args, _ = p.parse_known_args()

    _logger_conf(args)

    if not args.conf:
        p.print_usage()
        return

    c = _conf_init(args.conf)
    _task_launcher(c, args)
    return


if __name__ == "__main__":
    try:
        main()
    except Exception:
        ret = 1
        import traceback

        traceback.print_exc()
    sys.exit(ret)
